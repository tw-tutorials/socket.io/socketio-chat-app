import React from 'react';
import '../styles/chat.scss';

const onlineIcon = require('../icons/onlineIcon.png');
const closeIcon = require('../icons/closeIcon.png');

interface Props {
	room: string;
}
const InfoBar: React.FC<Props> = ({ room }) => (
	<div className="chat--info-bar">
		<div className="chat--left-inner-container">
			<img src={onlineIcon} alt="online" className="chat--online-icon" />
			<h3>{room}</h3>
		</div>
		<div className="chat--right-inner-container">
			<a href="/">
				<img src={closeIcon} alt="close" />
			</a>
		</div>
	</div>
);

export default InfoBar;
