import React, { useEffect, useState } from 'react';
import queryString from 'query-string';
import io from 'socket.io-client';
import { RouteProps } from 'react-router-dom';
import InfoBar from './InfoBar';
import Input from './Input';
import Messages from './Messages';

import '../styles/chat.scss';
import { IMessage } from '../interfaces';

interface Message {
	user: string;
	text: string;
}

let socket: SocketIOClient.Socket;

const Chat: React.FC<RouteProps> = ({ location }) => {
	const [name, setName] = useState('');
	const [room, setRoom] = useState('');
	const [message, setMessage] = useState('');
	const [messages, setMessages] = useState<IMessage[]>([]);

	const ENDPOINT = 'localhost:3001';

	useEffect(() => {
		const data: { name: string; room: string } = queryString.parse(location!.search) as any;
		const { name, room } = data;

		socket = io(ENDPOINT);
		console.log(socket);

		setName(name);
		setRoom(room);

		socket.emit('join', { name, room }, (error?: any) => {
			if (error) {
				console.error(error);
			}
		});

		return () => {
			socket.disconnect();
			socket.off(ENDPOINT);
		};
	}, [ENDPOINT, location]);

	// function for sending messages

	useEffect(() => {
		socket.on('message', (message: IMessage) => {
			setMessages([...messages, message]);
		});
	}, [messages]);

	const sendMessage = (
		event: React.KeyboardEvent<HTMLInputElement> | React.MouseEvent<HTMLButtonElement>
	) => {
		event.preventDefault();
		console.log(message);

		if (message) {
			socket.emit('sendMessage', message, () => {
				setMessage('');
			});
		}
	};

	return (
		<div className="chat--outer-container">
			<div className="chat--container">
				<InfoBar room={room} />
				<Messages messages={messages} name={name} />
				<Input message={message} setMessage={setMessage} sendMessage={sendMessage} />
			</div>
		</div>
	);
};

export default Chat;
