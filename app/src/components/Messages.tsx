import React from 'react';
import ScrollToBottom from 'react-scroll-to-bottom';
import Message from './Message';
import { IMessage } from '../interfaces';

import '../styles/chat.scss';

interface Props {
	messages: IMessage[];
	name: string;
}
const Messages: React.FC<Props> = ({ messages, name }) => {
	return (
		<ScrollToBottom className="messages">
			{messages.map((message, index) => (
				<div key={index}>
					<Message message={message} name={name} />
				</div>
			))}
		</ScrollToBottom>
	);
};

export default Messages;
