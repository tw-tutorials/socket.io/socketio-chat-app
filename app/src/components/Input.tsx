import React from 'react';
import '../styles/chat.scss';

interface Props {
	message: string;
	setMessage: (message: string) => void;
	sendMessage: (
		event: React.KeyboardEvent<HTMLInputElement> | React.MouseEvent<HTMLButtonElement>
	) => void;
}
const Input: React.FC<Props> = ({ message, setMessage, sendMessage }) => (
	<form className="form">
		<input
			className="input"
			type="text"
			placeholder="Type a message..."
			value={message}
			onChange={(e) => setMessage(e.target.value)}
			onKeyPress={(e) => (e.key === 'Enter' ? sendMessage(e) : null)}
		/>
		<button className="sendButton" onClick={(e) => sendMessage(e)}>
			Send
		</button>
	</form>
);

export default Input;
