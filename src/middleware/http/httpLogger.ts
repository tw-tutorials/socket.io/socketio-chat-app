import { Request, Response, NextFunction } from 'express';
import logger from '../../util/logger';
import { ConsoleColors } from '../../util/consoleColors';

const env = String(process.env.NODE_ENV).trim();

/**
 * Log all HTTP requests
 */
export default function httpLogger(req: Request, res: Response, next: NextFunction) {
	const start = Date.now();

	res.on('finish', () => {
		const duration = Date.now() - start;
		const method = req.method;
		const route = req.originalUrl;
		const status = res.statusCode;

		// Set color for status code
		let statusColor = ConsoleColors.Green;
		if (status >= 300 && status < 400) statusColor = ConsoleColors.Cyan;
		if (status >= 400 && status < 500) statusColor = ConsoleColors.Yellow;
		if (status >= 500) statusColor = ConsoleColors.Red;

		const bytesSent = req.get('content-length') || 0;
		const bytesReceived = res.get('content-length') || 0;

		if (env === 'dev') {
			// dev environment: apply colors
			logger.http(
				`${ConsoleColors.Magenta}${method}${ConsoleColors.Reset}` +
					` ${ConsoleColors.White}${route}${ConsoleColors.Reset}` +
					` ${statusColor}${status}${ConsoleColors.Reset}` +
					` ${ConsoleColors.Blue}${duration} ms${ConsoleColors.Reset}` +
					` - ${ConsoleColors.Reset}${bytesSent}${ConsoleColors.Red}↑ ${ConsoleColors.Reset}${bytesReceived}${ConsoleColors.Green}↓`
			);
		} else {
			logger.http(
				`${method} ${route} ${status} ${duration} ms - ${bytesSent}/${bytesReceived}`
			);
		}
	});

	return next();
}
