import fs from 'fs';
import path from 'path';
import express, { Application, Request, Response } from 'express';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import httpLogger from './middleware/http/httpLogger';
import apiRoutes from './routes/routes';

// Create new express application
const app: Application = express();

// Remove x-powered-by
app.disable('x-powered-by');

// Middleware
app.use(cors());
app.use(httpLogger);
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/api', apiRoutes);

//#region Serve react app
app.use(express.static(path.join(__dirname, '..', 'app', 'build')));
// Fallback for react router
app.get('*', (req: Request, res: Response) => {
	const appPath = path.join(__dirname, '..', 'app', 'build', 'index.html');

	if (fs.existsSync(appPath)) return res.sendFile(appPath);
	else return res.redirect('/');
});
//#endregion

export default app;
