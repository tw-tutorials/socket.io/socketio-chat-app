import express from 'express';
const api = express.Router();

import apiRoutes from './api';

api.use('/', apiRoutes);

export default api;
