import express from 'express';
import { getApiStatus } from '../controllers/api';
const api = express.Router();

api.get('/', getApiStatus);
api.get('/status', getApiStatus);

export default api;
