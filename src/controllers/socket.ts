import logger from '../util/logger';
import { addUser, removeUser, getUser, getUsersInRoom, User } from '../util/users';
import { io } from '../bin/start';
import { userInfo } from 'os';

export function onConnection(socket: SocketIO.Socket) {
	logger.verbose('We have a new connection!');

	socket.on('join', (props: { name: string; room: string }, callback?: Function) => {
		try {
			const { name, room } = props;

			const newUser = addUser({ id: socket.id, name, room });
			logger.verbose(`User '${name}' joined room '${room}'`);

			socket.emit('message', {
				user: 'admin',
				text: `${newUser.name}, welcome to the room ${newUser.room}`,
			});
			socket.broadcast
				.to(newUser.room)
				.emit('message', { user: 'admin', text: `${newUser.name} has joined!` });

			socket.join(newUser.room);

			io.to(newUser.room).emit('roomData', {
				room: newUser.room,
				users: getUsersInRoom(newUser.room),
			});

			if (callback) callback();
		} catch (error) {
			if (callback) {
				return callback(error);
			}
		}
	});

	socket.on('sendMessage', (message: string, callback: Function) => {
		const user = getUser(socket.id);
		if (!user) return;

		io.to(user.room).emit('message', { user: user.name, text: message });
		logger.silly(`${user.name}: ${message}`);

		io.to(user.room).emit('roomData', { room: user.room, users: getUsersInRoom(user.room) });

		callback();
	});

	socket.on('disconnect', () => {
		logger.verbose('User had left!');
		const user = removeUser(socket.id);

		if (user) {
			io.to(user.room).emit('message', { user: 'admin', text: `${user.name} has left.` });
		}
	});
}
