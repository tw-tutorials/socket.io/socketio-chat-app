import { Request, Response } from 'express';
import * as packageJson from '../../package.json';
import { io } from '../bin/start';
import logger from '../util/logger';

/**
 * Get API status, name, author and version number
 */
export function getApiStatus(req: Request, res: Response) {
	logger.debug('[controller]: getApiStatus');

	const { name, description, author, version } = packageJson;

	io.emit('message', { user: 'admin', text: `Version: ${version}` });

	return res.status(200).send({
		name,
		description,
		author,
		api: 'online',
		version,
	});
}
