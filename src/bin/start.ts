console.clear();
import logger from '../util/logger';
logger.debug('Starting application...');

import app from '../app';
import debug from 'debug';
import http from 'http';
import socketio from 'socket.io';
import { onConnection } from '../controllers/socket';

// Get port from environment and store in Express
const port = normalizePort(process.env.API_PORT || '3001'); // Use 3001 as fallback port
app.set('port', port);

// Create HTTP server
const server = http.createServer(app);

// Initialize socket.io
export const io = socketio(server);
io.on('connection', onConnection);

// Listen on provided port, on all network interfaces
server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

//SECTION Functions

/**
 * Normalize a port into a number, string or false
 */
function normalizePort(value: string): number | string | false {
	const port = parseInt(value, 10);

	// check if port is a number
	if (isNaN(port)) {
		// named pipe
		return value;
	}

	// check if port is a valid number
	if (port >= 0) {
		return port;
	}

	// fallback
	return false;
}

/**
 * Event listener for HTTP server "error" event
 * @param {any} err Error
 */
function onError(err: any) {
	if (err.syscall !== 'listen') {
		throw err;
	}

	const bind = typeof port === 'string' ? `Pipe ${port}` : `Port ${port}`;

	// handle specific listen errors with friendly messages
	switch (err.code) {
		case 'EACCES':
			logger.error(`${bind} requires elevated privileges`);
			process.exit(1);
		case 'EADDRINUSE':
			logger.error(`${bind} is already in use`);
			process.exit(1);
		default:
			throw err;
	}
}

/**
 * Event listener for HTTP server "listening" event
 */
async function onListening() {
	const address = server.address();
	const bind = typeof address === 'string' ? `pipe ${address}` : `port ${address?.port}`;

	debug(`Listening on ${bind}...`);
	logger.info(`Listening on ${bind}...`);
}

//!SECTION
