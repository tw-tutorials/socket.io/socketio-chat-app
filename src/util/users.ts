export interface User {
	id: string;
	room: string;
	name: string;
}

const users: User[] = [];

export function addUser(user: User) {
	const name = user.name.trim().toLowerCase();
	const room = user.room.trim().toLowerCase();

	const existingUser = users.find((u) => u.room === room && u.name === name);
	if (existingUser) throw new Error('Username is taken');

	users.push(user);
	return user;
}

export function removeUser(id: string) {
	const index = users.findIndex((u) => u.id === id);
	if (index !== -1) {
		return users.splice(index, 1)[0];
	}
}

export function getUser(id: string) {
	return users.find((u) => u.id === id);
}

export function getUsersInRoom(room: string) {
	return users.filter((u) => u.room === room);
}
