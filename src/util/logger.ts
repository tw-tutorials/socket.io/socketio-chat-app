import winston, { format } from 'winston';

const formatWithTimestamp = format.printf(({ level, message, timestamp }) => {
	return `[${timestamp}] ${level}: ${message}`;
});

/**
 * Logger
 *
 * Log levels: *
 * 0: error: red
 * 1: warn: yellow
 * 2: info: green
 * 3: http: magenta
 * 4: verbose: blue
 * 5: debug: cyan
 * 6: silly: grey
 */

const logLevels = {
	levels: {
		error: 0,
		warn: 1,
		info: 2,
		http: 3,
		db: 4,
		verbose: 5,
		debug: 6,
		silly: 7,
	},
	colors: {
		error: 'red',
		warn: 'yellow',
		info: 'green',
		http: 'magenta',
		db: 'grey',
		verbose: 'blue',
		debug: 'cyan',
		silly: 'grey',
	},
};

interface Logger extends winston.Logger {
	db: winston.LeveledLogMethod;
}

const logger = winston.createLogger({
	level: 'silly',
	levels: logLevels.levels,
	transports: [],
}) as Logger;

const NODE_ENV = String(process.env.NODE_ENV).trim();

// save error logs only
logger.add(
	new winston.transports.File({
		format: format.combine(format.timestamp(), formatWithTimestamp),
		level: 'error',
		filename: 'logs/error.log',
	})
);

// save all logs
logger.add(
	new winston.transports.File({
		format: format.combine(format.uncolorize(), format.timestamp(), formatWithTimestamp),
		level: 'debug',
		filename: 'logs/debug.log',
	})
);

// log to console if not in production
if (NODE_ENV !== 'production') {
	// log to console
	logger.add(
		new winston.transports.Console({
			format: format.combine(
				format.cli({
					levels: logLevels.levels,
					colors: logLevels.colors,
					level: true,
					message: true,
				}),
				format.simple()
			),
		})
	);
}

export default logger;
